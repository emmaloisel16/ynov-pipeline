def call(){
    pipeline {
        agent any
        stages {
            stage('Gradle spotlessCheck') {
                steps {
                    bat 'gradlew spotlessCheck'
                }
            }
            stage('Gradle Build') {
                steps {
                    bat 'gradlew clean build'
                }
            }
            stage('Gradle publish'){
                steps {
                    bat 'gradlew publish'
                }
            }
            stage('Gradle Push to Docker') {
                steps {
                    bat 'gradlew jib'
                }
            }

        }
        post {
            always {
                archiveArtifacts artifacts: 'build/libs/*.jar', fingerprint: true
            }
        }
    }
}
